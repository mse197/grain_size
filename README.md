# README #

To install the scikit-image package for handling pictures, do:

```
#!bash
$ pip install --install-option="--prefix=${HOME}/software" scikit-image
```
import numpy as np
from matplotlib import pyplot as plt
from matplotlib import image as mi
from scipy import ndimage as ndi
from skimage import io, data, morphology, measure, exposure
from skimage.color import rgb2gray
from skimage.segmentation import random_walker
from skimage.feature import peak_local_max
from skimage.filters import threshold_otsu, threshold_adaptive
from sympy import *
from skimage.morphology import watershed
from skimage.morphology import disk
from skimage.morphology import opening, closing, dilation
from random import randint

#Importing image
image = io.imread('samples/Good Examples/Good_Example_3.png')

#read in an image, and try to make the dark parts dark and the light parts light
#since we are trying to find the size of each grain, and grains are separated by 
#dark parts

# Changing image from RGB type to grayscale
image = rgb2gray(image)
image = image[0:1200,:] # get rid of scale bar part of image
io.imsave('output/0start.png',image)
image = exposure.equalize_adapthist(image,clip_limit=0.03) #increase contrast
io.imsave('output/1contrast.png',image)
selem = morphology.disk(5)
image = morphology.closing(image,selem) #cut out some holes
io.imsave('output/2main.png',image)

#Make some copies of the main image, for use later in the code...
distances = np.copy(image)
mask = np.copy(image) #

#In this part we make the "mask" image, which we want to be black where there are definitely
#not grains, and white where there probably is a grain. It's OK for there to be extra white,
#we'll try to separate out separate grains using image segmentation below
selem = morphology.disk(9)
mask = morphology.opening(mask,selem)  #clear up bright spots in mask
mask[mask<.7]=0
mask[mask>0]=1 #turn mask binary
selem = morphology.disk(5)
mask = morphology.dilation(mask,selem) #these lines each dilate the white area into the black more
mask = morphology.dilation(mask,selem)
mask = morphology.dilation(mask,selem)
io.imsave('output/3mask.png',mask)

#In this part we make the "islands" image, which we want to have one "island" for each grain.
#Basically, we want to throw away so much of the original image, that there is only one blob
#for each individual grain.  We will use the islands as the starting points for figuring out
#our grain sizes.
#image[image<.9]=0 #Turn pixels less than 90% brightness black
image[image<.9]=0
image[image>0]=1 #Turn all other pixels white
selem = morphology.disk(12)
islands = morphology.opening(image,selem) #clean up bright spots
io.imsave('output/4islands.png',islands) #island starting pgoints

#Here we perform image segmentation: Basically, we are going to pour water onto the -distance
#transform of the mask, but only onto the pixels labeled markers.  As the holes in the -distance
#transform fill up and run into each other, the boundaries stop growing, which is used as the 
#criteria for segmenting the different grains.
markers = ndi.label(islands)[0]
distance = ndi.distance_transform_edt(mask)
labels = morphology.watershed(-distances, markers, mask=mask)
mi.imsave('output/5grains.png',labels,cmap=plt.cm.spectral) #save the distance transform 

#Print out information about each grain.
mask2 = np.zeros(labels.shape)
mask2[labels>0]=1
number_labels = labels.max()
print number_labels

#Counting Objects
from scipy.ndimage import measurements,morphology


#labels, nbr_objects = measurements.label(image)
#print "Number of grains:", nbr_objects

#Asks user to enter the number on the scalebar
#scalelength = raw_input(("Enter the number on the scalebar in um "))

#Asks user to enter the length of the scalebar
#actualscale = raw_input(("Enter the length of the scalebar in mm "))

#Asks user to enter the width of the image
#picturewidth = raw_input(("Enter the width of the image in mm "))

#Asks user to enter the height of the image
#pictureheight = raw_input(("Enter the height of the image in mm "))

#Calculating vertical grain size
#x = symbols("x")
#vgrainsize = x / nbr_objects
#vgrainsize1 = vgrainsize.subs(x, pictureheight)
#vgrainsize2 = vgrainsize1.evalf()
#print vgrainsize2

#Calculating horizontal grain size
#y = symbols("y")
#hgrainsize = y / nbr_objects
#hgrainsize1 = hgrainsize.subs(y, picturewidth)
#hgrainsize2 = hgrainsize1.evalf()
#print hgrainsize2

#Calculating avg grain size in mm
#a, b = symbols("a b")
#avg = (a + b)/ 2
#avg1 = avg.subs([(a, vgrainsize2), (b, hgrainsize2)])
#avg2 = avg1.evalf()
#print "Average grain size: ", avg2 

#Calculating um/grain based on magnification
#z, w, q  = symbols("z w q")
#actualgrain = z * ((x/q))
#actualgrain1 = actualgrain.subs([(z, avg2), (x, scalelength), (q, actualscale)])
#actualgrain2 = actualgrain1.evalf()
#print "Average grain size: ", actualgrain2


#Print out information about each grain.
mask2 = np.zeros(labels.shape)
mask2[labels>0]=1
number_labels = labels.max()
print number_labels
sizes = ndi.sum(mask,labels,range(1,number_labels))
actualsizemm  = (27*sizes)/2080
actualsizeum = (actualsizemm*100)/6.2
#sizes[x] has the number of pixels in the grain with index x.
print sizes
print actualsizeum


averagesize = actualsizeum.mean()
print averagesize


# Importing plotting and image analysis modules
 
from matplotlib import pyplot as plt
from skimage.color import rgb2gray
from skimage import io
from sympy import *
#CHANGING IMAGE TO GRAYSCALE
# Importing image 
image = io.imread('samples/Good Examples/Good_Example_1.png')
# Changing image from RGB type to grayscale
image = rgb2gray(image)
#image = image[0:-100] #chop off the bottom 100 lines to get rid of the scale bar
# Taking the average value of all the pixels in the image
m = image.mean()
#print m
# Setting all pixels with a value much less than the average value to 0. Turns these regions of the image black.
image[image<m/1]=0.
# Saves image as test.png
io.imsave('output/test21.png',image)

#EDGE DETECTION
#from scipy import ndimage
#from skimage import feature

# Compute the Canny filter for image
#edges = feature.canny(image, sigma=3)

# Saves image as testedges.png
#io.imsave('output/testedges.png',edges)

#COUNTING OBJECTS
from scipy.ndimage import measurements,morphology


labels, nbr_objects = measurements.label(image)
print "Number of grains:", nbr_objects

#Asks user to enter the number on the scalebar
scalelength = raw_input(("Enter the number on the scalebar in um "))

#Asks user to enter the length of the scalebar
actualscale = raw_input(("Enter the length of the scalebar in mm "))

#Asks user to enter the width of the image
picturewidth = raw_input(("Enter the width of the image in mm "))

#Asks user to enter the height of the image
pictureheight = raw_input(("Enter the height of the image in mm "))

#Calculating vertical grain size
x = symbols("x")
vgrainsize = x / nbr_objects
vgrainsize1 = vgrainsize.subs(x, pictureheight)
vgrainsize2 = vgrainsize1.evalf()
#print vgrainsize2

#Calculating horizontal grain size
y = symbols("y")
hgrainsize = y / nbr_objects
hgrainsize1 = hgrainsize.subs(y, picturewidth)
hgrainsize2 = hgrainsize1.evalf()
#print hgrainsize2

#Calculating avg grain size in mm
a, b = symbols("a b")
avg = (a + b)/ 2
avg1 = avg.subs([(a, vgrainsize2), (b, hgrainsize2)])
avg2 = avg1.evalf()
#print "Average grain size: ", avg2 

#Calculating um/grain based on magnification
z, w, q  = symbols("z w q")
actualgrain = z * ((x/q))
actualgrain1 = actualgrain.subs([(z, avg2), (x, scalelength), (q, actualscale)])
actualgrain2 = actualgrain1.evalf()
print "Average grain size: ", actualgrain2


#io.imsave('output/mask.png',mask*255) #multiply by 255 to make the 1's turn white.

# load image and threshold to make sure it is binary
#labels, nbr_objects = measurements.label(mask)
#print "Number of objects:", nbr_objects
#io.imsave('output/labels.png',labels)



